#include "../Managers/AnimationManager.cpp";
#include "../Managers/FileSystemManager.cpp";
#include "../Managers/MemoryManager.cpp";
#include "../Managers/PhysicsManager.cpp";
#include "../Managers/RenderManager.cpp";
#include "../Managers/TextureManager.cpp";
#include "../Managers/VideoManager.cpp";


RenderManager        gRenderManager;
PhysicsManager       gPhysicsManager;
AnimationManager     gAnimationManager;
TextureManager       gTextureManager;

VideoManager         gVideoManager;
MemoryManager        gMemoryManager;
FileSystemManager    gFileSystemManager;

int main(int artc, const char* argv)
{
    //Start up engine systems in the CORRECT order.
    gMemoryManager.start_up();
    gFileSystemManager.start_up();
    gVideoManager.start_up();
    gTextureManager.start_up();
    gRenderManager.start_up();
    gAnimationManager.start_up();
    gPhysicsManager.start_up();

    //Run the game.
    //gSimulationManager.run();

    //Shut everything down in the REVERSE order.
    gPhysicsManager.shut_down();
    gAnimationManager.shut_down();
    gRenderManager.shut_down();
    gTextureManager.shut_down();
    gVideoManager.shut_down();
    gFileSystemManager.shut_down();
    gMemoryManager.shut_down();

    return 0;
}
